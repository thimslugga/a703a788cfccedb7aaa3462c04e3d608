#!/bin/bash

# Check if script was run as non-root user
function is_root() {
  if [[ ${EUID} -ne 0 ]]; then
    echo "This script must be run as root."
    exit 1
  fi
}
is_root

# Make sure the rsyslog package is installed
rpm -q rsyslog || zypper in -y rsyslog

# Make sure the the logd service is enabled and started
systemctl -q is-active logd.service || systemctl enable --now logd.service

# Cleanup any prior external/ec2.bak that already exists and are lingering around
#test -f /usr/lib64/stonith/plugins/external/ec2.bak && rm -f "/usr/lib64/stonith/plugins/external/ec2.bak"

# Before modification, make a backup of the external/ec2 stonith agent script
test -f /usr/lib64/stonith/plugins/external/ec2 && cp /usr/lib64/stonith/plugins/external/ec2{,.bak}

# Add the 'set -xv' to the script
sed -i '2iset -xv' /usr/lib64/stonith/plugins/external/ec2

# Add the aws-cli --debug flag to the script
sed -i 's/options="/options="--debug /' /usr/lib64/stonith/plugins/external/ec2